FROM exadra37/phoenix:1.3 as build_phoenix_release

ENV REPLACE_OS_VARS=true MIX_ENV=prod RELEASE_PATH="${CONTAINER_HOME}"/release

ARG APP_NAME

COPY --chown=elixir:elixir app/"${APP_NAME}" .

RUN mkdir -p "${RELEASE_PATH}" && \
    chown "${CONTAINER_USER}":"${CONTAINER_USER}" "${RELEASE_PATH}" && \
    ls -al && \
    rm -rf node_modules deps _build && \
    mix deps.get --only prod && \
    npm install && \
    mix compile && \
    node_modules/.bin/brunch build --production && \
    mix phoenix.digest && \
    mix release && \
    set -xe; \
        RELEASE_TARBALL_PATH=$(find _build/${MIX_ENV}/rel/*/releases -maxdepth 2 -name '*.tar.gz') && \
        RELEASE_TARBALL_FILENAME="${RELEASE_TARBALL_PATH##*/}" && \
        cp ${RELEASE_TARBALL_PATH} ${RELEASE_PATH}/${RELEASE_TARBALL_FILENAME} && \
        cd ${RELEASE_PATH} && \
        mkdir ${APP_NAME} && \
        tar -xzf ${RELEASE_TARBALL_FILENAME} -C ${APP_NAME} && \
        rm ${RELEASE_TARBALL_FILENAME}

FROM erlang:20-alpine

ARG RELEASE_PATH=/home/elixir/release
ARG APP_NAME

ENV REPLACE_OS_VARS=true

RUN apk update && \
    apk upgrade && \
    apk add bash && \
    addgroup -g 1000 -S phoenix && \
    adduser -u 1000 -S phoenix -G phoenix && \
    mkdir -p /home/phoenix/${APP_NAME} && \
    chown phoenix:phoenix /home/phoenix/${APP_NAME}

USER phoenix

WORKDIR /home/phoenix/${APP_NAME}

COPY --chown=phoenix:phoenix --from=build_phoenix_release ${RELEASE_PATH}/${APP_NAME} /home/phoenix/${APP_NAME}

# APP_NAME as an ARG is only availabe when we build the image, thus we need to set it as an environment variable
#  in order to have it available when we want to run a container from this image.
ENV APP_NAME=${APP_NAME}

CMD bin/${APP_NAME} foreground
