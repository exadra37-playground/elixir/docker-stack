# ELIXIR PHOENIX DOCKER STACK

The base for development in all my Elixir/Phoenix projects.


## 1. HOW TO INSTALL

### 1.1 Clone the Elixir Docker Stack

```bash
git clone git@gitlab.com:exadra37-playground/elixir/docker-stack.git project-name && cd project-name && ./stack --setup-stack
```

### 1.2 The ENV File

Is imperative that the environment variable `APP_NAME` have the same application
name used when we created the application with `mix new my-app-name`.

```
APP_NAME=my-app-name
```

## 2. HOW TO USE

If you have not done yet is imperative that you run first:

```bash
./stack --setup-stack
```


### 2.1 ELIXIR

#### 2.1.1 IEX Shell

```bash
./stack run elixir-iex
```

#### 2.1.2 CLI Shell

```bash
./stack run elixir-shell
```

#### 2.1.3 HTTP for Development

```bash
./stack up elixir-http-dev
```

Visit http://localhost:4005


### 2.2 PHOENIX


#### 2.2.1 IEX Shell

```bash
./stack run phoenix-iex
```

#### 2.2.2 CLI Shell

```bash
./stack run phoenix-shell
```

#### 2.2.3 HTTP for Development

```bash
./stack up phoenix-http-dev
```

Visit http://localhost:4006


### 2.3 DATABASE CLI

The awesome a pretty cli for Postgres is the default one. More details on the
[official website](https://www.pgcli.com/).

The option `--user` can be only one of:

* `database-dev`
* `database-test`
* `database-prod`

```bash
./stack run database-cli --host database-dev --user user-name database-name
```


### 2.4 DATABASE BACKUPS

#### 2.4.1 Create a backup

```bash
./stack exec database-dev pg_dump -U user-name database-name > file-name.sql
```

#### 2.4.2 Import the backup

First we need to ensure the sql file with the dump is located on the host path
as per configured in the docker compose file for the database service volume,
that is something like `~/.elixir/${APP_NAME}/database/dev/postgresql/data`.

```bash
./stack exec database-dev psql -U user-name -d database-name -f /var/lib/postgresql/data/file-name.sql
```


### 2.5 OBSERVER

```bash
./stack elixir-iex
```

Now in the iex shell...

#### 2.5.1 Starting the Observer

```bash
iex(2)> :observer.start
:ok
```

Click in the tab **View** and after in **Refresh interval** and change the
refresh interval from 10 seconds to 1 second.

Playing with IEX and watching the Observer Tabs...

```bash
iex(3)> Enum.each(1..5, fn number -> spawn(fn -> number * number |> IO.puts end) end)
1
4
9
16
25
:ok
iex(4)>
```

The above is to quick for us to notice anything!!!

Instead of the previous 5 processes lets try to spawn 1 million of processes...

```bash
iex(4)> Enum.each(1..1000000, fn number -> spawn(fn -> number * number |> IO.puts end) end)
```

Now we have time to see all the metrics in the **System** and **Load Charts** tabs.

See how despite 1 million processes the memory usage does not go above 60MB!!!

In Elixir processes are very cheap, each one costs around `0.3kb` plus the data
they will need to handle.
